import numpy as np
import networkx as nx
import os

MxPL=2
rpath='./Rule'
pr_triple_file='DistMult_Predicted.txt'  #line format is [h r t]
out_exp='Explain_path.txt'

def ret_paths(Gr, src, trg):
    paths=nx.all_simple_edge_paths(Gr, source=src, target=trg, cutoff=MxPL)
    pathlist = []
    exrulelist = []
    for path in paths:
        singlepath = []
        singlerule = []
        #compute the path score
        for triple in path:
            (h,t,r)=triple
            singlepath.append(h)
            singlepath.append(r)  
            singlepath.append(t)  
            singlerule.append(r)
            
        pathlist.append(singlepath)
        exrulelist.append(singlerule)
        
    return pathlist, exrulelist


#for building dictionaries
print('Loading entities , relations......')
#load the entity and relation list
E = {}
R = {}
R1= {}

with open('entity2id.txt','r') as flr:
    line=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (name, idd)=line.strip().split('\t')
        E[int(idd)]=name
        line=flr.readline()

with open('relation2id.txt','r') as flr:
    line=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (name, idd)=line.strip().split('\t')
        R[int(idd)+1]=name    #adjust index:add +1 to avoid +0, -0 case for inverse relation
        R1[name]=int(idd)+1    #adjust index:add +1 to avoid +0, -0 case for inverse relation
        line=flr.readline()

#for building graph
print('Building graph.........')
Hs = []
Ts = []
Rs = []
#build the graph
Graph = nx.MultiDiGraph()
#add nodes
for ent in list(E.keys()):
    Graph.add_node(ent)

with open('train2id.txt','r') as flr:
    hd=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (h,t,r)=line.strip().split(' ')
        h=int(h)
        t=int(t)
        r=int(r)+1  #adjust index:add +1 to avoid +0, -0 case for inverse relation
        
        Hs.append(h)
        Ts.append(t)
        Rs.append(r)
        
        line=flr.readline()
        
with open('valid2id.txt','r') as flr:
    hd=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (h,t,r)=line.strip().split(' ')
        h=int(h)
        t=int(t)
        r=int(r)+1  #adjust index:add +1 to avoid +0, -0 case for inverse relation
        
        Hs.append(h)
        Ts.append(t)
        Rs.append(r)
        
        line=flr.readline()   
        
L=len(Hs)
for i in range(L):
    h=Hs[i]
    t=Ts[i]
    r=Rs[i]
    
    Graph.add_edge(h, t, key=r)
    
    e=Graph.get_edge_data(h, t, default=-100000) # get the relation between h->t
    if(e!=-100000): # if there is at least one edge
        rv=list(e.keys())
        for oldr in rv:    
            if(oldr<0):   # if the inverse edge is false edge
                Graph.remove_edge(h, t, key=oldr)  #remove the false edge and add true one
 
    e=Graph.get_edge_data(t, h, default=-100000) # get the relation between t->h
    if(e==-100000): # if there is no reverse edge
        Graph.add_edge(t, h, key=-r)         #add a false reverse edge t, -1, h 
    else:
        rv=list(e.keys())
        flag=1
        for oldr in rv:    
            if(oldr>=0):   # if the inverse edge is true edge
                flag=0
        if(flag==1):
            Graph.add_edge(h, t, key=-r)  #if no positive edge then add a false reverse edge

#for reading mined rules
#Rule	 Count	 Rank	 Support	 HC	 BC
SYMLIST = ['A','B','C','D','E','F','G']
#read the rules
for r in list(R.keys()):
    print('Explanining paths for the relation: '+R[r])
    #read the rules
    Ruleset = []
    Rankset = []
    fll=os.path.join(rpath, 'Rule_'+str(r)+'.csv')
    with open(fll,'r') as flr:
        head=flr.readline()
        line=flr.readline()
        while(len(line)>0):
            singlerule = []
            (Rule, Count, Rank, Support, HC, BC)=line.strip().split('\t')
            (body,head)=Rule.split('=>')
            Rankset.append(int(Rank))
            ATMS=body.split(' AND ')
            for atm in ATMS:
                (name,sms)=atm.split('(')
                rid=R1[name]
                (sym1,sym2)=sms.replace(')','').split(',')
                if(SYMLIST.index(sym1)>SYMLIST.index(sym2)):
                    rid=-rid
                    
                singlerule.append(rid)
                
            Ruleset.append(singlerule)
            
            line=flr.readline()

    #open the predicted file and find the best path        
    with open(pr_triple_file,'r') as flr:
        #header=flr.readline()
        line=flr.readline()
        while(len(line)>0):
            (h,rorg,t)=line.strip().split(' ')
            (h,t,rorg)=(int(h),int(t),int(rorg)+1)
            if(rorg==r):
                pathlist, exrulelist=ret_paths(Graph, h, t)
                bestrank=1000000
                bestpath = []
                for rule in exrulelist:
                    if(rule in Ruleset):   #if the new path in mined rule set
                        ind=Ruleset.index(rule)
                        if(Rankset[ind]<bestrank):   #if the new path satisfies better ranked rule
                            bestrank=Rankset[ind]
                            pind=exrulelist.index(rule)
                            bestpath=pathlist[pind]
                            
                #write the best path if found           
                if(bestrank<1000000):
                    flw=open(out_exp,'a')
                    LL=int(len(bestpath)/3)
                    
                    head=R[rorg]+'('+E[h]+','+E[t]+')'
                    body=''
                    for ind in range(LL):
                        h1=bestpath[ind*3]  
                        r1=bestpath[(ind*3)+1]
                        t1=bestpath[(ind*3)+2]
                        if(r1>0):
                            body=body+' AND '+R[r1]+'('+E[h1]+','+E[t1]+')'
                        else:
                            body=body+' AND '+R[-r1]+'('+E[t1]+','+E[h1]+')' 
                            
                    body = body[4:]
                    exp = body+'=>'+head
                    flw.writelines(exp+'\n')
                    flw.close()
            

            line=flr.readline()    

print('Finished')    

