import numpy as np
import torch.nn.functional as fn
import torch
import networkx as nx
import numpy as np
import timeit
import os

rstart=0
sample_rate=0.5
rstop=10
MxPL=2
sample_topk=5
entemb='DistMult_Em_Ent.txt'
relemb='DistMult_Em_Rel.txt'
Dim=100

def forward(head, tail, rela, EM1, RM1):
    shape = head.size()
    head = head.contiguous().view(-1)
    tail = tail.contiguous().view(-1)
    rela = rela.contiguous().view(-1)
    
    head_embed = fn.normalize(EM1[head],2,-1)
    tail_embed = fn.normalize(EM1[tail],2,-1)
    rela_embed = fn.normalize(RM1[rela],2,-1)
    return torch.norm(tail_embed - head_embed - rela_embed, p=1, dim=-1).view(shape)

def score(head, tail, rela, EM1, RM1):
        return forward(head, tail, rela, EM1, RM1)

def ret_paths(Gr, src, trg, rel, EM1, RM1):
    paths=nx.all_simple_edge_paths(Gr, source=src, target=trg, cutoff=MxPL)
    pathlist = []
    scrlist = []
    for path in paths:
        path_score=0
        singlepath = []
        headtail = []
        dr = []
        #compute the path score
        for triple in path:
            (h,t,r)=triple
            #singlepath.append(h)
            singlepath.append(r)  
            headtail.append((h,t))
            #singlepath.append(t)     
            #compute the triple score
            h=torch.tensor([h])
            t=torch.tensor([t])
            if(r<0):
                r=(-1)*r
                r=torch.tensor([r])
                sc=score(t,h,r-1,EM1, RM1)  #adjust index
            else:
                r=torch.tensor([r])
                sc=score(h,t,r-1,EM1, RM1)   #adjust index

            path_score=path_score+torch.flatten(sc).detach().cpu().numpy()
            
        path_score=path_score/pow(len(singlepath),1.25)
            
        #if path length<3 then add a dummy self-loop (relid=500000 represents self-loop)
        for i in range(MxPL-len(singlepath)):
            singlepath.append(500000)
            
        #if multiple same paths exists then pick the top scoring one
        if(singlepath in pathlist):
            ind=pathlist.index(singlepath)
            pv=scrlist[ind]
            if(pv<path_score[0]):  #if current score>previous score then update score of rule
                scrlist[ind]=path_score[0]
        else:
            pathlist.append(singlepath)
            scrlist.append(path_score[0])
            
    finalpathlist = []     
    for i in range(len(pathlist)):
        sp = pathlist[i]
        sp.append(scrlist[i])
        finalpathlist.append(sp)
            
        #singlepath.append(path_score[0])
        #print(singlepath)
        #pathlist.append(singlepath)
        
    a = np.array(finalpathlist)    
    if(len(pathlist)>=1):
        a=a[a[:,MxPL].argsort()[::-1]]
        
    return a



rstart=rstart+1   #id adjustment
rstop=rstop+1     #id adjustment

print('Loading entities , relations and embedding')
#load the entity and relation list
E = {}
R = {}

with open('entity2id.txt','r') as flr:
    line=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (name, idd)=line.strip().split('\t')
        E[int(idd)]=name
        line=flr.readline()

with open('relation2id.txt','r') as flr:
    line=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (name, idd)=line.strip().split('\t')
        R[int(idd)+1]=name    #adjust index:add +1 to avoid +0, -0 case for inverse relation
        line=flr.readline()

#Get the embedding
NE=len(E)
NR=len(R)
#load entity embeddings
EM = np.zeros((NE,Dim))
with open(entemb,'r') as flr:
    for idd in range(0,NE):
        #print('Scanning entity: '+str(idd))
        vec = []
        totalstring=flr.readline()
        while(']' not in totalstring):
            line=flr.readline()
            totalstring=totalstring+' '+line.strip()           
        
        totalstring=totalstring.replace('[','').replace(']','').strip()
        arr=totalstring.split(' ')
        for e in arr:
            if(len(e)>0):
                vec.append(float(e))
                
        if(len(vec)==Dim):
            for i in range(Dim):
                EM[idd,i]=vec[i]
        else:
            print(idd)
            break

#load relation embeddings
RM = np.zeros((NR,Dim))
with open(relemb,'r') as flr:
    for idd in range(0,NR):
        #print('Scanning relation: '+str(idd))
        vec = []
        totalstring=flr.readline()
        while(']' not in totalstring):
            line=flr.readline()
            totalstring=totalstring+' '+line.strip()           
        
        totalstring=totalstring.replace('[','').replace(']','').strip()
        arr=totalstring.split(' ')
        for e in arr:
            if(len(e)>0):
                vec.append(float(e))
                
        if(len(vec)==Dim):
            for i in range(Dim):
                RM[idd,i]=vec[i]
        else:
            print(idd)
            break

#turn np array to tensor
EM=torch.from_numpy(EM)
RM=torch.from_numpy(RM)

print('Building graph')
Hs = []
Ts = []
Rs = []
#build the graph
Graph = nx.MultiDiGraph()
#add nodes
for ent in list(E.keys()):
    Graph.add_node(ent)

with open('train2id.txt','r') as flr:
    hd=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (h,t,r)=line.strip().split(' ')
        h=int(h)
        t=int(t)
        r=int(r)+1  #adjust index:add +1 to avoid +0, -0 case for inverse relation
        
        Hs.append(h)
        Ts.append(t)
        Rs.append(r)
        
        line=flr.readline()
        
with open('valid2id.txt','r') as flr:
    hd=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (h,t,r)=line.strip().split(' ')
        h=int(h)
        t=int(t)
        r=int(r)+1  #adjust index:add +1 to avoid +0, -0 case for inverse relation
        
        Hs.append(h)
        Ts.append(t)
        Rs.append(r)
        
        line=flr.readline()   
        
L=len(Hs)
for i in range(L):
    h=Hs[i]
    t=Ts[i]
    r=Rs[i]
    
    Graph.add_edge(h, t, key=r)
    
    e=Graph.get_edge_data(h, t, default=-100000) # get the relation between h->t
    if(e!=-100000): # if there is at least one edge
        rv=list(e.keys())
        for oldr in rv:    
            if(oldr<0):   # if the inverse edge is false edge
                Graph.remove_edge(h, t, key=oldr)  #remove the false edge and add true one
 
    e=Graph.get_edge_data(t, h, default=-100000) # get the relation between t->h
    if(e==-100000): # if there is no reverse edge
        Graph.add_edge(t, h, key=-r)         #add a false reverse edge t, -1, h 
    else:
        rv=list(e.keys())
        flag=1
        for oldr in rv:    
            if(oldr>=0):   # if the inverse edge is true edge
                flag=0
        if(flag==1):
            Graph.add_edge(h, t, key=-r)  #if no positive edge then add a false reverse edge

for rorg in range(rstart,rstop+1):
    print('Learning rules for relation: '+str(R[rorg]))
    L=len(Hs)
    Hr = []
    Rr = []
    Tr = []
    #extract totoal triples with the rulation rorg
    for i in range(L):
        h=Hs[i]
        t=Ts[i]
        r=Rs[i]
        if(r==rorg): #if the relation is found
            Hr.append(h)
            Rr.append(r)
            Tr.append(t)

    Ruleset = []
    SC = []
    EN = []
    CNT = []
    NTH=2
    EN_TH=10

    #randomly pick 50% sample
    RI = np.arange(len(Hr))
    np.random.shuffle(RI)
    K=int(len(Hr)*sample_rate)
    print('Total number of triples to scan: '+str(K))

    #learning rules
    print('learning..')
    start = timeit.default_timer()
    for ind in range(K):
        i=RI[ind]
        #print('Processing: '+str(ind)+'/'+str(K),end='\r')
        h=Hr[i]
        t=Tr[i]
        r=Rr[i]

        #remove the edge and its inverse (if added)
        Graph.remove_edge(h, t, key=r)
        #e=Graph.get_edge_data(t, h, default=-100000)
        #rv=list(e.keys())
        #if(-r in rv):
        #    Graph.remove_edge(t, h, key=-r) 

        #extract the paths
        pts=ret_paths(Graph, h, t, r, EM, RM)
        pts1=pts

        row=0
        column=0
        if(pts.size>0):
            (row,column)=np.shape(pts)

        flag  = []
        topk=row

        #scan all paths and update rule info if already in ruleset        
        for j in range(row):
            rule_new=[(pts1[j,0],pts1[j,1])]#,pts1[j,2])]
            if(rule_new in Ruleset):
                ind1=Ruleset.index(rule_new)
                #pts = np.delete(pts, (j-1), axis=0)       #remove the rule from extracted set as it is process
                CNT[ind1]=CNT[ind1]+1
                SC[ind1]=((SC[ind1]*(CNT[ind1]-1))+pts1[j-1,column-1])/CNT[ind1] #update avg score
                EN[ind1]=EN_TH+1

                flag.append(1)
                topk=topk-1
            else:
                flag.append(0)

        if((ind>EN_TH) and (topk>sample_topk)):
            topk=sample_topk

        #scan all paths and add new rule info if found
        for j in range(row):
            rule_new=[(pts1[j,0],pts1[j,1])]#,pts1[j,2])]
            if(flag[j]==0):
                Ruleset.append(rule_new)           
                CNT.append(1.0)
                SC.append(pts1[j-1,column-1]) 
                EN.append(EN_TH+1)
                topk=topk-1

            if(topk<0):
                break


        N_Rule=len(EN)
        noise=0
        for j in range(N_Rule-1,-1,-1):
            EN[j]=EN[j]-1
            if(EN[j]<0 and CNT[j]<NTH):
                EN.pop(j)
                CNT.pop(j)
                SC.pop(j)
                Ruleset.pop(j)
                noise=noise+1

    #    with open('Removed_'+R[rorg]+'.txt','a') as flw:
    #        flw.writelines(str(i)+'\t'+str(noise)+'\n')

        #add edge and its reverse  
        Graph.add_edge(h, t, key=r)         #this for symmetric relation
        #if(-r in rv):
        #    Graph.add_edge(t, h, key=-r) #temporarily remove the negtive edge (if added)

    stop = timeit.default_timer()
    print('Total Time: ', stop - start)  

    #ranks the rules
    Rank = []
    for k in range(len(CNT)):
        Rank.append(1000)   #default value
    for rank in range(len(CNT)):
        maxcnt=-1
        ind=-1
        for k in range(len(CNT)):
            if((CNT[k]>maxcnt) and (Rank[k]==1000)):
                maxcnt=CNT[k]
                ind=k

        Rank[ind]=rank

    #transform path into rules
    FinalRule = []
    FinalRuleString = []
    FRank = []
    FCNT = []
    Support = []
    Lit = ['A','B','C','D']
    print('Rule sets for: '+R[rorg])

    for i in range(len(CNT)):
        if(CNT[i]>=NTH):
            rule=Ruleset[i][0]
            FinalRule.append(rule)
            FCNT.append(CNT[i])
            Support.append(0)
            rl=''
            ls='B'
            for j in range(MxPL):
                r1=rule[j]
                if(r1>=0 and r1<500000):
                    rl=rl+' AND '+R[r1]+'('+Lit[j]+','+Lit[j+1]+')'
                    ls=Lit[j+1]
                elif(r1<0):
                    r1=-r1
                    rl=rl+' AND '+R[r1]+'('+Lit[j+1]+','+Lit[j]+')'
                    ls=Lit[j+1]

            rl=rl[4:]
            ts=rl+'=>'+R[rorg]+'(A,'+str(ls)+')'
            FinalRuleString.append(ts)
            FRank.append(Rank[i])
            #print(ts+':: '+str(CNT[i])+' '+str(Rank[i]))


    print('Total number of rules: '+str(len(FinalRule)))

    #Compute the support
    print('Computing support')
    for i in range(len(Hr)):
        #print('Processing: '+str(i),end='\r')
        h=Hr[i]
        t=Tr[i]
        r=Rr[i]

        #remove the edge and its inverse (if added)
        Graph.remove_edge(h, t, key=r)
        #e=Graph.get_edge_data(t, h, default=-100000)
        #rv=list(e.keys())
        #if(-r in rv):
        #    Graph.remove_edge(t, h, key=-r) 

        #extract the paths
        pts=ret_paths(Graph, h, t, r, EM, RM)
        row=0
        if(pts.size>0):
            (row,column)=np.shape(pts)

        #scan all paths and update rule info if already in ruleset        
        for j in range(row):
            rule_new=[(pts[j,0],pts[j,1])]#,pts[j,2])]
            if(rule_new[0] in FinalRule):
                ind=FinalRule.index(rule_new[0])
                Support[ind]=Support[ind]+1


        #add edge and its reverse  
        Graph.add_edge(h, t, key=r)         #this for symmetric relation
        #if(-r in rv):
        #    Graph.add_edge(t, h, key=-r) #temporarily remove the negtive edge (if added)

    #write into files
    print('Computing HC....')
    HC = []
    #print(FinalRule)
    for j in range(len(Support)):
        hcc=Support[j]/len(Hr)
        HC.append(hcc)


    print('Computing SC.....')
    BC = []
    for i in range(len(FinalRule)):
        #print('Processing: '+str(i),end='\r')
        rule=FinalRule[i]
        BC.append(0)
        Pairs=set()
        TNDS=list(Graph.nodes())
        for nd in TNDS:
            newp=(nd,nd)
            Pairs.add(newp)

        for l in range(MxPL):
            r=int(rule[l])
            if(r==500000):
                break

            tmppairs = set()
            for pair in Pairs:
                (head,tail)=pair
                oeds=list(Graph.out_edges(nbunch=tail,data=True, keys=True))
                for ed in oeds:
                    (h1,t1,r1,vtmp)=ed
                    if(r1==r):
                        newp=(head,t1)
                        tmppairs.add(newp)

            Pairs=tmppairs

        BC[i]=len(Pairs)


    print('Writing rules to file')
    path = "./Rule"
    with open(os.path.join(path, "Rule_"+str(rorg)+".csv"),'a') as flw:
        flw.writelines('Rule\t Count\t Rank\t Support\t HC\t BC\n')
        for j in range(len(Support)):
            flw.writelines(FinalRuleString[j]+'\t'+str(FCNT[i])+'\t'+str(FRank[j])+'\t'+str(Support[j])+'\t'+str(HC[j])+'\t'+str(round(Support[j]/BC[j],3))+'\n')

    print('Finished')




