# KGLP
This repository gives the source code of our paper "Negative sampling and rule mining for explainable link prediction in knowledge graphs". This repository has four parts: SNS sampling, Rule mining, abductive reasoning, and negative checker. Please follow the intructions below to run the experiments.

## Prepare the dataset
For a knowledge graph (KG), the following five files are generated.

1. The entity dictionary in "entity2id.txt": The first line is the total number of distinct entities, and the following lines are formatted in the form "entity-name id" (separated by a single tab), which describes an entity name and its numerical identifier (starting from 0).

2. The relation dictionary in "relation2id.txt’: The first line is the total number of distinct relations, and the following lines are formatted in the form "relation-name id" (separated by a single tab), which describes a relation name and its numerical identifier (starting from 0).

3. The train triples in "train2id.txt": The first line is the total number of train triples, and the following lines are formatted in the form "head-id tail-id relation-id" (separated by a single space).

4. The valid triples in "valid2id.txt": The file is formatted in the same way as the "train2id.txt" file.
5. The test triples in "test2id.txt": The file is formatted in the same way as the "train2id.txt" file.

## SNS sampling to learning embeddings and link predictions

A knowledge graph embedding model with SNS sampling is used to learn embeddings. The embeddings are used to test the model on the test triples in the test2id.txt file.

The embedding files are written in the dataset directory. For example, DistMult_Em_Ent.txt gives the entity embeddings and DistMult_Em_Rel.txt gives the relation embeddings for DistMult model training. Besides, the positively predicted test triples (hit@10=1) are written in the ‘Model_Predicted.txt’ (e.g. DistMult_Predicted.txt for DistMult model) file in the data set directory.

Note: Currently, we are working solving license issues to make the source code open to public.

## Rule mining
The source code for mining rules is available in the "Rule_mining" folder. To run the experiments, please follow the following steps.

1. Use ‘Mining2.py’ file to learn length-2 rules, ‘Mining3.py’ file to learn length-3 rules, ‘Mining4.py’ to learn length-4, and ‘Mining5.py’ to learn length-5 rules.
2. Put all the dataset files (entity2id.txt, relation2id.txt, train2id.txt, test2id.txt, valid2id.txt) and embedding files (e.g. DistMult_Em_Ent.txt and DistMult_Em_Rel.txt for DistMult model) in the same directory as the python script.
3. Set the following parameters in the correct python script. 

| Variable      | Type    | Default-value         | Description                                              |
| ------------- | ------- | --------------------- | -------------------------------------------------------- |
| rstart        | int     | 0                     | The relation id to start (id can be found in the relation2id.txt file) |
| rstop         | int     | 0                     | The relation id to end   (id can be found in the relation2id.txt file) |
| sample_rate	| float	  | 0.5                   | The sampling rate [>0.0 and <1.0] |
| sample_topk   | int     | 5                     | The top-k value |
| entemb        | str     | DistMult_Em_Ent.txt   | The entity embeddings file  |
| relemb        | str     | DistMult_Em_Ent.txt   | The relation embeddings file |
| Dim           | int     | 100                   | The embedding size |


4. Create a sub-folder "Rule" to store the mined rules.
5. Run the python script to mine rules. At the end of learning rules, one rule file for each relation will be generated. The file will contain the rules with their quality metrics.


## Rule mining
The source code for mining rules is available in the "Rule_mining" folder. To run the experiments, please follow the following steps.

1. Use ‘Mining2.py’ file to learn length-2 rules, ‘Mining3.py’ file to learn length-3 rules, ‘Mining4.py’ to learn length-4, and ‘Mining5.py’ to learn length-5 rules.
2. Put all the dataset files (entity2id.txt, relation2id.txt, train2id.txt, test2id.txt, valid2id.txt) and embedding files (e.g. DistMult_Em_Ent.txt and DistMult_Em_Rel.txt for DistMult model) in the same directory as the python script.
3. Set the following parameters in the correct python script. 

| Variable      | Type    | Default-value         | Description                                              |
| ------------- | ------- | --------------------- | -------------------------------------------------------- |
| rstart        | int     | 0                     | The relation id to start (id can be found in the relation2id.txt file) |
| rstop         | int     | 0                     | The relation id to end   (id can be found in the relation2id.txt file) |
| sample_rate	| float	  | 0.5                   | The sampling rate [>0.0 and <1.0] |
| sample_topk   | int     | 5                     | The top-k value |
| entemb        | str     | DistMult_Em_Ent.txt   | The entity embeddings file  |
| relemb        | str     | DistMult_Em_Ent.txt   | The relation embeddings file |
| Dim           | int     | 100                   | The embedding size |


4. Create a sub-folder "Rule" to store the mined rules.
5. Run the python script to mine rules. At the end of learning rules, one rule file for each relation will be generated. The file will contain the rules with their quality metrics.

## Abductive reasoning

This experiment gives the most possible path which triggers the correct predictions by an embedding model (written in ‘Predicted.txt’ file). The source code for mining rules is available in the "Explanation" folder. To run the experiment, please follow the following steps.

1. Put all the dataset files (entity2id.txt, relation2id.txt, train2id.txt, test2id.txt, valid2id.txt), embedding files (e.g. DistMult_Em_Ent.txt and DistMult_Em_Rel.txt for DistMult model), the mined rules in a "Rule" sub-folder and the positively predicted test triple file (e.g. DistMult_Predicted.txt for DistMult model) file in a directory.
2. Set the following parameters in the "Explanation.py" script.

| Variable         | Type    | Default-value           | Description                                              |
| -------------    | ------- | ---------------------   | -------------------------------------------------------- |
| MxPL             | int     | 2                       | The maximum path length |
| rpath            | str     | ./Rule                  | The rules directory |
| pr_triple_file   | str	 | DistMult_Predicted.txt  | The positively predicted triple by embedding model #line format is [h r t] |
| out_exp          | str     | Explain_path.txt        | The file where the triggering path for each triple will be written  | 

3. In the "Explain_path.txt" file, the most possible triggering path for a triple is written.

| Variable         | Type    | Default-value           | Description                                              |
| -------------    | ------- | ---------------------   | -------------------------------------------------------- |
| MxPL             | int     | 2                       | The maximum path length |
| rpath            | str     | ./Rule                  | The rules directory |
| pr_triple_file   | str	 | DistMult_Predicted.txt  | The positively predicted triple by embedding model #triple format is [h r t] |
| out_exp          | str     | Explain_path.txt        | The file where the triggering path for each triple will be written  | 

## Negative triple checker

This experiment gives the negatives generated by the negative checker. The source code for negative checker is available in the "Checker" folder. To run the experiment, please follow the following steps.

1. Put all the dataset files (entity2id.txt, relation2id.txt, train2id.txt, test2id.txt, valid2id.txt), embedding files (e.g. DistMult_Em_Ent.txt and DistMult_Em_Rel.txt for DistMult model).
2. Set the following parameters in the "Negative_Checker.py" script.

| Variable         | Type    | Default-value           | Description                                              |
| -------------    | ------- | ---------------------   | -------------------------------------------------------- |
| N1               | int     | 50                      | The size of candidate negative set |
| N2               | int     | 5                       | The top-N2 in SNS (N2<=N1) |
| nsample          | int     | 1                       | How many negatives will be generated for each positive (nsample<=N2) |
| model   	   | str     | DistMult                | The knowledge graph embedding model (e.g. DistMult, TransH) |
| Pos              | string  | Positives.txt           | The input positive triples file. The file is formatted in the same way as train2id.txt |
| Neg              | string  | Negatives.txt           | The output negative triples file. The file is formatted in the same way as train2id.txt |

In the output file, each line gives a list of negatives seperated by commas for each positive in the input file.
