import numpy as np
import torch
import torch.nn.functional as F
import numpy as np
import random

model='DistMult' #[DistMult, TransH]
N1=50
N2=3
nsample=1 #for pair-wise training
lrsfile='LRS.txt'
Pos='train2id.txt'
Neg='Negatives.txt'
useLRS=True


print('Loading entities , relations and embedding')
#load the entity and relation list
E = {}
R = {}

with open('entity2id.txt','r') as flr:
    line=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (name, idd)=line.strip().split('\t')
        E[int(idd)]=name
        line=flr.readline()

with open('relation2id.txt','r') as flr:
    line=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        (name, idd)=line.strip().split('\t')
        R[int(idd)+1]=name    #adjust index:add +1 to avoid +0, -0 case for inverse relation
        line=flr.readline()

#Get the embedding
NE=len(E)
NR=len(R)
Dim=100
#load entity embeddings
EM = np.zeros((NE,Dim))
with open(model+'_Em_Ent.txt','r') as flr:
    for idd in range(0,NE):
        #print('Scanning entity: '+str(idd))
        vec = []
        totalstring=flr.readline()
        while(']' not in totalstring):
            line=flr.readline()
            totalstring=totalstring+' '+line.strip()           
        
        totalstring=totalstring.replace('[','').replace(']','').strip()
        arr=totalstring.split(' ')
        for e in arr:
            if(len(e)>0):
                vec.append(float(e))
                
        if(len(vec)==Dim):
            for i in range(Dim):
                EM[idd,i]=vec[i]
        else:
            print(idd)
            break

EM=torch.from_numpy(EM)
#load relation embeddings
RM = np.zeros((NR,Dim))
with open(model+'_Em_Rel.txt','r') as flr:
    for idd in range(0,NR):
        #print('Scanning relation: '+str(idd))
        vec = []
        totalstring=flr.readline()
        while(']' not in totalstring):
            line=flr.readline()
            totalstring=totalstring+' '+line.strip()           
        
        totalstring=totalstring.replace('[','').replace(']','').strip()
        arr=totalstring.split(' ')
        for e in arr:
            if(len(e)>0):
                vec.append(float(e))
                
        if(len(vec)==Dim):
            for i in range(Dim):
                RM[idd,i]=vec[i]
        else:
            print(idd)
            break
            
RM=torch.from_numpy(RM)

#load relation embeddings
W = np.zeros((NR,Dim))
if(model=='TransH'):
    with open('./SNS/TransH_Em_Rel_M.txt','r') as flr:
        for idd in range(0,NR):
            #print('Scanning relation: '+str(idd))
            vec = []
            totalstring=flr.readline()
            while(']' not in totalstring):
                line=flr.readline()
                totalstring=totalstring+' '+line.strip()           

            totalstring=totalstring.replace('[','').replace(']','').strip()
            arr=totalstring.split(' ')
            for e in arr:
                if(len(e)>0):
                    vec.append(float(e))

            if(len(vec)==Dim):
                for i in range(Dim):
                    RM[idd,i]=vec[i]
            else:
                print(idd)
                break
                
    W=torch.from_numpy(W)
#load LRS if used
def load_LRS():
    H_LRS1 = {}
    T_LRS1 = {}
    with open(lrsfile,'r') as flr:
        line=flr.readline()
        while(len(line)>0):
            (triple,headlrs,taillrs)=line.strip().split('\t')
            (h,t,r)=triple.strip().split(',')
            (h,t,r)=(int(h),int(t),int(r))
            heads = []
            headarr=headlrs.split(',')
            for en in headarr:
                if(len(en)>0):
                    en=int(en)
                    heads.append(en)
            H_LRS1[(h,t,r)]=heads  

            tails = []
            tailarr=taillrs.split(',')
            for en in tailarr:
                if(len(en)>0):
                    en=int(en)
                    tails.append(en)
            T_LRS1[(h,t,r)]=tails 
            line=flr.readline()
            
    return H_LRS1, T_LRS1

#compute sampling probabilities

def candidate(ent1, ent2, rela):
    shape = ent1.size()
    ent1 = ent1.contiguous().view(-1)
    ent2 = ent2.contiguous().view(-1)
    rela = rela.contiguous().view(-1)
    ent1_embed = F.normalize(EM[ent1], 2, -1)
    ent2_embed = F.normalize(EM[ent2], 2, -1)
    vec=torch.norm(ent1_embed - ent2_embed, p=2, dim=-1).view(shape)
    vec=torch.abs(vec)
    return vec

def prob(head, tail, rela):
    vec = candidate_transh(head, tail, rela)
    vec = -vec/2.0
    return F.softmax(vec, dim=-1)

#the SNS sampling
def neg_sample_SNS(head, tail, rela, h_LRS, t_LRS):
    n_ent=len(E)
    if(useLRS==True):
        h_cand = np.concatenate([[h_LRS], np.random.choice(n_ent, (1, N1))], 1)
        t_cand = np.concatenate([[t_LRS], np.random.choice(n_ent, (1, N1))], 1)
    else:
        h_cand = np.random.choice(n_ent, (1, N1+nsample))
        t_cand = np.random.choice(n_ent, (1, N1+nsample))
    
    h_cand = torch.from_numpy(h_cand).type(torch.LongTensor)
    t_cand = torch.from_numpy(t_cand).type(torch.LongTensor)

    # expand for computing scores/probs
    rela_h = rela.unsqueeze(1).expand(-1, N1+nsample)
    tail_h = tail.unsqueeze(1).expand(-1, N1+nsample)
    head_t = head.unsqueeze(1).expand(-1, N1+nsample)
    rela_t = rela.unsqueeze(1).expand(-1, N1+nsample)

    
    h_probs = prob(h_cand, tail_h, rela_h)
    t_probs = prob(head_t, t_cand, rela_t)
    _, h_new = torch.topk(h_probs,  k=N2, dim=-1)
    _, t_new = torch.topk(t_probs,  k=N2, dim=-1)
    
    ind=torch.arange(1).unsqueeze(1)
    h_idx=h_cand[ind,h_new]
    t_idx=t_cand[ind,t_new]

    h_ind = np.random.randint(low=0, high=N2, size=(1, nsample))
    t_ind = np.random.randint(low=0, high=N2, size=(1, nsample))
    
    h_idx = h_idx[0, h_ind]
    t_idx = t_idx[0, t_ind]
    
    #return the entities array, distance array, probabilities array, the samples entity
    return  h_idx, t_idx

#return the final negative
def return_neg(h,r,t, h_LRS, t_LRS):
    (h1,t1,r1)=(h,t,r)   
    head=torch.tensor([h])
    rela=torch.tensor([r])
    tail=torch.tensor([t])
    h_idx, t_idx = neg_sample_SNS(head, tail, rela, h_LRS, t_LRS)

    h_idx = h_idx.numpy()[0]
    t_idx = t_idx.numpy()[0]
    
    #final selection of negative
    totalneg = []
    for i in range(nsample):
        totalneg.append((h_idx[i],t1,r1))
        totalneg.append((h1,t_idx[i],r1))
        
    negatives = random.sample(totalneg, nsample)
    
    return negatives

#The main run
H_LRS = {}
T_LRS = {}
if(useLRS==True):
    print('Loading LRS...')
    H_LRS, T_LRS = load_LRS()
i=0
flw=open(Neg,'a')
with open(Pos,'r') as flr:
    head=flr.readline()
    line=flr.readline()
    while(len(line)>0):
        print('Generating negative(s): '+str(i),end='\r')
        (h,t,r)=line.strip().split()
        nl=h+','+t+','+r
        (h,t,r)=(int(h),int(t),int(r))
        h_LRS = []
        t_LRS = []
        if(useLRS==True):
            h_LRS = np.array(H_LRS[(h,t,r)])
            t_LRS = np.array(T_LRS[(h,t,r)])
        negatives = return_neg(h,r,t, h_LRS, t_LRS)
        for ng in negatives:
            (nh,nt,nr)=ng
            nl=nl+'\t'+str(nh)+','+str(nt)+','+str(nr)

        flw.writelines(nl+'\n')
        line=flr.readline()
        i=i+1
flw.close()
print('Finished')
